//
// Created by tzhou on 2/21/18.
//

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <map>
#include <set>
#include <backtrace.h>
#include <backtrace-supported.h>
#include "../CCECommons/Commons.hpp"
#include "CtxRecord.hpp"

using namespace std;

#define OptimizeStack

const int ctx_size = 128;
const int pushed_size = ctx_size + sizeof(short);

extern "C" {
extern short xps_ctx[ctx_size];
short xps_ctx[ctx_size];
}

//extern "C" __thread size_t xps_ctx;

// Anonymous namespace
namespace {

int _next = 0;
vector<void*> _ctx_stack;
vector<int> _level_stack;
vector<pair<size_t, size_t>> _short_ctx_stack;
map<string, string> _ctxs;
map<string, string> _cvs;
backtrace_state *_bt_state;
int _tz = 0;

struct CtxRecord {
  short data[ctx_size];
  CtxRecord() {
    for (int i = 0; i < ctx_size; ++i) {
      data[i] = 0;
    }
  }

  void getImmediateCtx(int level) {
    int bytes = sizeof(short) * level;
    memcpy(data, xps_ctx, bytes);
  }

  bool equals(CtxRecord &record) {
    for (int i = 0; i < ctx_size; ++i) {
      if (data[i] != record.data[i]) {
        return false;
      }
    }
    return true;
  }

  string str(int level) {
    string s;
    for (int i = 0; i < level; ++i) {
      if (i) {
        s += "_";
      }
      char chars[10];
      sprintf(chars, "%04x", data[i]);
      s += std::to_string(i) + "_" + chars;
    }
    return s;
  }

  string getCtxAsStr(int level) {
    string ctx;
    getImmediateCtx(level);
    ctx = "    " + str(level);

    for (int i = 0; i < _next; ++i) {
      short* p = (short*)_ctx_stack[i];
      int bytes = p[0];
      memcpy(data, p+1, bytes);
      ctx += "\n\n";
      //ctx += "(" + to_string(_level_stack[i]) + ") @";
      ctx += std::to_string(i) + ": " + str(bytes/sizeof(short));
    }
    return ctx;
  }
};

string getBTCtxAsStr1() {
  const char* name = "backtrace.txt";
  FILE* f = fopen(name, "w");
  backtrace_print(_bt_state, 0, f);
  fclose(f);

  string ctx;
  string line;
  ifstream ifs(name);
  while (getline(ifs, ctx)) {
    ctx += line;
  }
  ifs.close();

  return ctx;
}

void reportCtxCollision(string &ctx, string &cv) {
  cerr << "Context collision: \n";
  cerr << ctx << "\n"
       << _ctxs[ctx] << "\n"
       << cv << "\n";

  backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
  cerr << "Cv collision: \n";
  cerr << cv << "\n"
       << _cvs[cv] << "\n"
       << ctx << "\n";
  backtrace_print(_bt_state, 2, stderr);
}

void checkCtx(int level) {
  string ctx = getBTCtxAsStr();
  CtxRecord cr;
  string cv = cr.getCtxAsStr(level);

  if (_tz < 0) {
    cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
    _tz++;
  }

  if (_ctxs.find(ctx) != _ctxs.end()) {
    if (cv != _ctxs[ctx]) {
      reportCtxCollision(ctx, cv);
    }
  } else {
    _ctxs[ctx] = cv;
  }

  if (_cvs.find(cv) != _cvs.end()) {
    if (ctx != _cvs[cv]) {
      reportCvCollision(cv, ctx);
    }
  } else {
    _cvs[cv] = ctx;
  }
}

void report() {
  cout << "ctxs: " << _ctxs.size() << "\n"
       << "cvs: " << _cvs.size() << "\n"
       << "max stack: " << _ctx_stack.size() << "\n"
       << "stack next: " << _next << "\n";
}

}


//=----------------- Marena Interfaces ---------------=//

extern "C" {
void init_acce() {
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);
}

void exit_acce() {
  report();
}

void acce_check_ctx(int level) {
  checkCtx(level);
}


//=----------------- IR Interfaces ---------------=//

//void acce_push_long_ctx(short start, short len, int bytes) {
//  if (_next == _ctx_stack.size()) {
//    _ctx_stack.push_back(malloc(pushed_size));
//  }
//
//  short* p = (short*)_ctx_stack[_next];
//
//  p[0] = start;
//  p[1] = len;
//  memcpy(p+2, xps_ctx+start, bytes);
//
//  _next++;
//}
//
//void acce_pop_long_ctx(int bytes) {
//  short* top = (short*)_ctx_stack[_next-1];
//  short start = top[0];
//  short len = top[1];
//  memcpy(xps_ctx+start, top+sizeof(short), bytes);
//  _next--;
//}

void acce_push_long_ctx(short start, short len) {
  size_t* addr = (size_t*)(xps_ctx + start);
  size_t compound_id = addr[0];
  _short_ctx_stack.push_back(make_pair(start, compound_id));

  for (int i = 4; i < len; i += 4) {
    size_t* addr = (size_t*)(xps_ctx + start + i);
    size_t compound_id = addr[i];
    _short_ctx_stack.push_back(make_pair(-1, compound_id));
  }
}

void acce_pop_long_ctx(short start, short len) {
  auto pa = _short_ctx_stack[_short_ctx_stack.size()-1];
  size_t* addr = (size_t*)(xps_ctx + start);
  addr[0] = pa.second;

  for (int i = 4; i < len; i += 4) {
    auto pa = _short_ctx_stack[_short_ctx_stack.size()-1];
    size_t* addr = (size_t*)(xps_ctx + start + i);
    addr[i] = pa.second;
  }
}

void acce_push_short_ctx(short start) {
  size_t* addr = (size_t*)(xps_ctx + start);
  size_t compound_id = addr[0];
  _short_ctx_stack.push_back(make_pair(start, compound_id));
}

void acce_pop_short_ctx(short start) {
  auto pa = _short_ctx_stack[_short_ctx_stack.size()-1];
  size_t* addr = (size_t*)(xps_ctx + start);
  addr[0] = pa.second;
}


void acce_push_ctx(int bytes) {
  if (_next == _ctx_stack.size()) {
    _ctx_stack.push_back(malloc(pushed_size));
  }

  short* p = (short*)_ctx_stack[_next];

    p[0] = bytes;
    memcpy(p+1, xps_ctx, bytes);

  _next++;
}

void acce_pop_ctx(int bytes) {
  void* top = _ctx_stack[_next-1];
  memcpy(xps_ctx, top+sizeof(short), bytes);
  _next--;
}



}
