//
// Created by tzhou on 3/8/18.
//

#include <fstream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <backtrace.h>
#include <backtrace-supported.h>
#include "marena.hpp"
#include "../CCECommons/Commons.hpp"
#include "../PCCE/Interface.hpp"

// ACCE
typedef char CtxSlotType;
typedef short EdgeType;
typedef size_t EdgeSlotType;

const int ctx_size = 64;
extern "C" {
CtxSlotType scce_ctx[ctx_size];
EdgeType* scce_start = 0;
int scce_size = 0;
int scce_capacity = 0;
  size_t scce_cycle_ctx = 0;
  size_t scce_cycle_ctx_size = 0;

// Debug
size_t scce_stack_sizes[ctx_size];

}

namespace {  
// Buffered push
unordered_map<size_t, int> _ctx_hashes;
vector<size_t > _cycle_edge_buffers;
  vector<size_t > _scce_cycle_ctxs;
  int _cycle_buffer_capacity;

  struct ECCEIDComparator {
    bool operator()(const pair<size_t, vector<EdgeType >>& a, const pair<size_t, vector<EdgeType >>& b) const {
      if (a.first != b.first) {        
        return a.first < b.first;
      }

      auto& v1 = a.second;
      auto& v2 = b.second;

      // if (v1.size() < 4 || v2.size() < 4) {
      //   return v1 < v2;
      // }

      
      if (v1.size() != v2.size()) {
        return v1.size() < v2.size();
      }

      if (v1.size() == 0) {
        return false;
      }

      auto units = v1.size() / 4;
      auto newv1 = (size_t*)&v1[0];
      auto newv2 = (size_t*)&v2[0];
      for (int i = 0; i < units; ++i) {
        if (newv1[i] != newv2[i]) {
          return newv1[i] < newv2[i];
        }
      }


      for (int i = 4*units; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
          return v1[i] < v2[i];
        }
      }
      return false;
    }
  };
// Unbuffered push
vector<EdgeType > _cycle_edges;

  // Detection
  map<pair<size_t, vector<EdgeType >>, int, ECCEIDComparator> _ecce_ctx_pairs;
  map<tuple<int, size_t, size_t, vector<size_t>>, int> _fcce_ctx_pairs;
  //map<pair<size_t, vector<size_t>>, int> _fcce_ctx_pairs;
  //map<pair<size_t, vector<EdgeType >>, int> _ecce_ctx_pairs;

  map<pair<vector<CtxSlotType>, vector<EdgeType>>, int> _scce_ctx_pairs;

// Debug
map<string, string> _ctxs;
map<string, string> _cvs;
backtrace_state *_bt_state;
int _tz = 0;

// Stats
size_t _read_bytes[2] = {0};
  size_t _pushed = 0;

string getSCCECtxAsStr(int level) {
  //printf("1\n");
  string s;
  for (int i = 0; i < level; ++i) {
    if (i) {
      s += "_";
    }
    char chars[10];
    sprintf(chars, "%04x", scce_ctx[i]);
    s += std::to_string(i) + "_" + chars;
  }

  s += "_cycle_";
  //printf("2\n");

  for (int i = 0; i < scce_size; ++i) {
    if (i) {
      s += "_";
    }
    char chars[10];
    sprintf(chars, "%04x", scce_start[i]);
    s += chars;
  }
  //printf("3\n");
  return s;
}

extern "C" size_t pcce_id;
  
size_t detectSCCECtx(int level) {
  size_t bytes = 0;
  size_t hash = 0;

  for (int i = 0; i < level; i += 8/sizeof(CtxSlotType)) {
    size_t* p = (size_t*)(scce_ctx + i);
    hash ^= *p + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    //hash += *p;
  }

  int passes = scce_size / (8/sizeof(EdgeType));
  for (int i = 0; i < passes*(8/sizeof(EdgeType)); i += 8/sizeof(EdgeType)) {
    size_t* p = (size_t*)(scce_start + i);
    hash ^= *p + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    //hash += *p;
  }

  for (int i = passes*(8/sizeof(EdgeType)); i < scce_size; ++i) {
    hash ^= scce_start[i] + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    //hash += scce_start[i];
  }
  
  _ctx_hashes[hash] = 0;

  _read_bytes[0] += level;
  _read_bytes[1] += scce_size * 2;
  return hash;
}

size_t detectSCCECtx1(int level) {
  size_t bytes = 0;
  size_t hash = 0;

  vector<CtxSlotType> slots(scce_ctx, scce_ctx+level);
  auto key = pair<vector<CtxSlotType>, vector<EdgeType >>(slots, _cycle_edges);
  if (_scce_ctx_pairs.find(key) == _scce_ctx_pairs.end()) {
    _scce_ctx_pairs[key] = 0;
  }

  _read_bytes[0] += level;
  _read_bytes[1] += _cycle_edges.size() * 2;
  return hash;
}

// @deprecated
size_t detectECCECtx_() {
  size_t bytes = 0;
  size_t hash = 0;

  hash ^= pcce_id + 0x9e3779b9 + (hash << 6) + (hash >> 2);

  int passes = scce_size / (8/sizeof(EdgeType));
  for (int i = 0; i < passes*(8/sizeof(EdgeType)); i += 8/sizeof(EdgeType)) {
    size_t* p = (size_t*)(scce_start + i);
    hash ^= *p + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    //hash += *p;
  }

  for (int i = passes*(8/sizeof(EdgeType)); i < scce_size; ++i) {
    hash ^= scce_start[i] + 0x9e3779b9 + (hash << 6) + (hash >> 2);
    //hash += scce_start[i];
  }

  _ctx_hashes[hash] = 0;

  _read_bytes[0] += 8;
  _read_bytes[1] += scce_size * 2;
  return hash;
}

  size_t detectSCCEShiftCtx(int id) {
    size_t bytes = 0;

    //_scce_cycle_ctxs.push_back(scce_cycle_ctx);
    //tuple<size_t, size_t, size_t, vector<size_t>> key(pcce_id, scce_cycle_ctx, scce_cycle_ctx_size, _scce_cycle_ctxs);

    //make_pair(scce_cycle_ctx, pcce_id);
    //make_pair(pcce_id, make_pair(scce_cycle_ctx, _scce_cycle_ctxs));
    tuple<int, size_t, size_t, vector<size_t>> key(id, pcce_id, scce_cycle_ctx, _scce_cycle_ctxs);
    //pair<size_t, vector<size_t>> key(pcce_id, _scce_cycle_ctxs);

    if (_fcce_ctx_pairs.find(key) == _fcce_ctx_pairs.end()) {
      _fcce_ctx_pairs[key] = 0;
    }
    //_scce_cycle_ctxs.pop_back();
    

    _read_bytes[0] += 8;
    _read_bytes[1] += _scce_cycle_ctxs.size() * 8 + 8;

  }
  
size_t detectECCECtx() {
  size_t bytes = 0;
  size_t hash = 0;

  auto key = pair<size_t, vector<EdgeType >>(pcce_id, _cycle_edges);
  if (_ecce_ctx_pairs.find(key) == _ecce_ctx_pairs.end()) {
    _ecce_ctx_pairs[key] = 0;
  }

  _read_bytes[0] += 8;
  _read_bytes[1] += _cycle_edges.size() * 2;
  return hash;
}

void reportCtxCollision(string &ctx, string &cv) {
 cerr << "Context collision: \n";
 cerr << ctx << "\n"
      << _ctxs[ctx] << "\n"
      << cv << "\n";

 backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
 cerr << "Cv collision: \n";
 cerr << cv << "\n"
      << _cvs[cv] << "\n"
      << ctx << "\n";
 backtrace_print(_bt_state, 2, stderr);
}

void checkCtx(int level) {
 string ctx = getBTCtxAsStr();
 string cv = getSCCECtxAsStr(level);

 if (_tz < 0) {
   //cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
   //_tz++;
 }

 if (_ctxs.find(ctx) != _ctxs.end()) {
   if (cv != _ctxs[ctx]) {
     reportCtxCollision(ctx, cv);
   }
 } else {
   _ctxs[ctx] = cv;
 }

 if (_cvs.find(cv) != _cvs.end()) {
   if (ctx != _cvs[cv]) {
     reportCvCollision(cv, ctx);
   }
 } else {
   _cvs[cv] = ctx;
 }
}

  void reportCtxNum(ofstream& ofs) {
    printf("reportCtxNum\n");
    ofs << "[SCCE]\n";
    int points[15];
    for (auto i: _fcce_ctx_pairs) {
      auto key = i.first;
      auto point_id = get<0>(key);
      printf("");
      points[point_id]++;
    }

    for (int i = 0; i < 15; ++i) {
      ofs << "  " << i << ": " << points[i] + "\n";
    }
  }
  
void report() {
  ofstream ofs("SCCE.out");
  ofs << "[SCCE]\n";
  ofs << "  ctxs: " << _ctxs.size() << "\n"
       << "  cvs: " << _cvs.size() << "\n"
       << "  max stack: " << _scce_cycle_ctxs.capacity() << "\n"
      << "  final stack: " << _scce_cycle_ctxs.size() << "\n"
       << "  ecce hashes: " << _fcce_ctx_pairs.size() << "\n"
      << "  scce hashes: " << _scce_ctx_pairs.size() << "\n"
       << "  read ids: " << _read_bytes[0] << " " << _read_bytes[1] << "\n"
      << "  pushed: " << _pushed << "\n"
    ;

  //reportCtxNum(ofs);
  ofs.close();
}

}

extern "C" {

//=----------------- Marena Interfaces ---------------=//
void scce_alloc() {
  _cycle_edge_buffers.push_back(0);

  scce_start = (EdgeType*)&_cycle_edge_buffers[0];
  scce_capacity += sizeof(EdgeSlotType) / sizeof(EdgeType);
  //scce_top = scce_start + len;
  //scce_end = scce_top + 4;
  //printf("%p %p\n", scce_size, scce_capacity);
}

void scce_store(int edge_id) {
  scce_start[scce_size] = edge_id;
}

void scce_push_edge(short edge_id) {
  _cycle_edges.push_back(edge_id);
  _pushed += 1;
}

void scce_pop_edge() {
  _cycle_edges.pop_back();
}

void scce_left_shift(short edge_id) {
  if (scce_cycle_ctx_size == 4) {
    printf("push %p, size: %d\n", scce_cycle_ctx, _scce_cycle_ctxs.size());
    _scce_cycle_ctxs.push_back(scce_cycle_ctx);
    scce_cycle_ctx = 0;
    scce_cycle_ctx_size = 0;
  }
  scce_cycle_ctx <<= 16;
  scce_cycle_ctx += edge_id;
  //printf("add: %p\n", edge_id);
  scce_cycle_ctx_size++;
}

void scce_right_shift() {
  if (scce_cycle_ctx_size == 0) {
    assert(_scce_cycle_ctxs.size() > 0);
    scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
    _scce_cycle_ctxs.pop_back();
    scce_cycle_ctx_size = 4;
    printf("pop %p, size: %d\n", scce_cycle_ctx, _scce_cycle_ctxs.size());
  }
  
  scce_cycle_ctx >>= 16;
  scce_cycle_ctx_size--;
  //printf("remove: \n");
}

  // void scce_push_cycle_ctx(int edge_id) {
  //   //printf("push %d -> %d\n", _scce_cycle_ctxs.size(), _scce_cycle_ctxs.size()+1);
  //   //cout << "push " << _scce_cycle_ctxs.size() << endl;
  //   _cycle_buffer_capacity = scce_cycle_ctx_size;
  //   _scce_cycle_ctxs.push_back(scce_cycle_ctx);
  //   scce_cycle_ctx = 0;
  //   scce_cycle_ctx_size = 0;
    
  // }

  // void scce_pop_cycle_ctx() {
  //   //printf("pop %d -> %d\n", _scce_cycle_ctxs.size(), _scce_cycle_ctxs.size()-1);
  //   //cout << "pop " << scce_cycle_ctx_size << endl;
  //   if (_scce_cycle_ctxs.empty()) {
  //     return;
  //   }

  //   scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
  //   scce_cycle_ctx_size = _cycle_buffer_capacity;
  //   _scce_cycle_ctxs.pop_back();
    
  // }

  /*** For the Branch (Default) Method ***/
  void scce_push_cycle_ctx(int edge_id) {
    _cycle_buffer_capacity = scce_cycle_ctx_size;
    _scce_cycle_ctxs.push_back(scce_cycle_ctx);
    
    scce_cycle_ctx = edge_id;
    scce_cycle_ctx_size = 1;
    _pushed++;
  }

  void scce_pop_cycle_ctx() {    
    scce_cycle_ctx = _scce_cycle_ctxs[_scce_cycle_ctxs.size()-1];
    _scce_cycle_ctxs.pop_back();

    // Before the push, it should be full
    // Now set back to full
    scce_cycle_ctx_size = _cycle_buffer_capacity;
  }


void init_scce() {
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);

  //scce_alloc();
}

void exit_scce() {
  //report();
}



void scce_check_ctx(int lv) {
  checkCtx(lv);
  //checkSCCECtx(lv);
}

void scce_detect_ctx(int lv) {
  detectSCCECtx1(lv);
}

void ecce_detect_ctx(size_t id) {
  //detectECCECtx();
  detectSCCEShiftCtx(id);
}
//=----------------- IR Interfaces ---------------=//
}
