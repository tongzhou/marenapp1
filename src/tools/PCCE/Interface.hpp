//
// Created by tzhou on 3/8/18.
//

#ifndef LIBMARENA_INTERFACE_HPP
#define LIBMARENA_INTERFACE_HPP

#include "../CCECommons/Commons.hpp"

typedef size_t CtxIDType;
typedef short SiteIDType;

extern "C" {
extern CtxIDType pcce_id;
}


string getPCCECtxAsStr();


#endif //LIBMARENA_INTERFACE_HPP
