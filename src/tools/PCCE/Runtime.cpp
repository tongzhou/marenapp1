//
// Created by tzhou on 2/24/18.
//
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <set>
#include <utility>      // std::pair, std::make_pair
#include <backtrace.h>
#include <backtrace-supported.h>
#include "marena.hpp"
#include "Interface.hpp"
#include <cassert>

extern "C" {
CtxIDType pcce_id = 0;
}

namespace {
vector<pair<CtxIDType, SiteIDType>> _ctx_stack;
map<string, string> _ctxs;
map<string, string> _cvs;
set<string> _ctx_collisions;
set<string> _cv_collisions;
backtrace_state *_bt_state;
int _tz = 0;

unordered_map<size_t, int> _ctx_hashes;
  map<tuple<int, CtxIDType, vector<pair<CtxIDType, SiteIDType>>>, int> _ctx_pairs;
size_t _read_bytes[2] = {0};
size_t _pushed = 0;

void reportCtxCollision(string &ctx, string &cv) {
  cerr << "Context collision: \n";
  cerr << ctx << "\n"
       << _ctxs[ctx] << "\n"
       << cv << "\n";

  backtrace_print(_bt_state, 2, stderr);
}

void reportCvCollision(string &cv, string &ctx) {
  cerr << "Cv collision: \n";
  cerr << cv << "\n"
       << _cvs[cv] << "\n"
       << ctx << "\n";
  backtrace_print(_bt_state, 2, stderr);
}

void checkCtx() {
  string ctx = getBTCtxAsStr();
  string cv = getPCCECtxAsStr();

  // backtrace_print(_bt_state, 1, stderr);
  // fprintf(stderr, "\n\n");
  // return;

  if (_tz < 0) {
    //cout << "ctx: " << ctx << "cv: " << cv << "\n\n";
    //_tz++;
  }
//
  if (_ctxs.find(ctx) != _ctxs.end()) {
    if (cv != _ctxs[ctx]) {
      if (_ctx_collisions.find(ctx) == _ctx_collisions.end()) {
        //reportCtxCollision(ctx, cv);
      }
      _ctx_collisions.insert(ctx);

    }
  } else {
    _ctxs[ctx] = cv;
  }

  if (_cvs.find(cv) != _cvs.end()) {
    if (ctx != _cvs[cv]) {
      if (_cv_collisions.find(cv) == _cv_collisions.end()) {
        //reportCvCollision(cv, ctx);
      }
      _cv_collisions.insert(cv);

    }
  } else {
    _cvs[cv] = ctx;
  }
}
} // end namespace

string getPCCECtxAsStr() {
  string s;
  for (auto i: _ctx_stack) {
    s += to_string(i.first) + ", " + to_string(i.second) + " | ";
  }
  s += to_string(pcce_id);
  return s;
}

size_t detectPCCECtx() {
  size_t ctx = 0;
  size_t bytes = 0;
  for (auto i: _ctx_stack) {
    ctx ^= i.first + 0x9e3779b9 + (ctx << 6) + (ctx >> 2);
    ctx ^= i.second + 0x9e3779b9 + (ctx << 6) + (ctx >> 2);
  }
  ctx ^= pcce_id + 0x9e3779b9 + (ctx << 6) + (ctx >> 2);
  _ctx_hashes[ctx] = 0;

  //bytes = _ctx_stack.size() * 2 + 1;
  _read_bytes[0] += 8;
  _read_bytes[1] += _ctx_stack.size() * 10;
  //printf("bytes: %lld\n", _ctx_stack.size());
  return ctx;
}

size_t detectPCCECtx1(int id) {
  size_t hash = 0;
  size_t bytes = 0;

  auto key = tuple<int, CtxIDType,
                  vector<pair<CtxIDType, SiteIDType>>>
    (id, pcce_id, _ctx_stack);
  if (_ctx_pairs.find(key) == _ctx_pairs.end()) {
    _ctx_pairs[key] = 0;
    }
  //printf("stack size: %d\n", _ctx_pairs.size());
  

  //bytes = _ctx_stack.size() * 2 + 1;
  _read_bytes[0] += 8;
  _read_bytes[1] += _ctx_stack.size() * 10;
  //printf("bytes: %lld\n", _ctx_stack.size());
  return hash;
}

  void reportCtxNum(ofstream& ofs) {
    ofs << "[PCCE]\n";
    int points[15];
    for (auto i: _ctx_pairs) {
      auto key = i.first;
      auto point_id = get<0>(key);
      points[point_id]++;
    }

    for (int i = 0; i < 15; ++i) {
      ofs << "  " << i << ": " << points[i] + "\n";
    }
  }

void report() {
  ofstream ofs("PCCE.out");
  ofs << "[PCCE]\n"
      << "  ctxs: " << _ctxs.size() << "\n"
      << "  cvs: " << _cvs.size() << "\n"
      << "  ctx collisions: " << _ctx_collisions.size() << "\n"
      << "  cv collisions: " << _cv_collisions.size() << "\n"
      << "  max stack: " << _ctx_stack.capacity() << "\n"
      << "  final stack: " << _ctx_stack.size() << "\n"
      << "  hashes: " << _ctx_pairs.size() << "\n"
      << "  read ids: " << _read_bytes[0] << " " << _read_bytes[1] << "\n"
      << "  pushed: " << _pushed << "\n"
    ;

  reportCtxNum(ofs);
  ofs.close();
}


extern "C" {

//=----------------- Marena Interfaces ---------------=//
void init_pcce() {
  pcce_id = 0;
  _bt_state = backtrace_create_state(
      NULL, 1, bt_error_callback, NULL);
}

void exit_pcce() {
  report();
}

void pcce_check_ctx() {
  checkCtx();
}

void pcce_detect_ctx(size_t id) {
  //checkCtx();
  detectPCCECtx1(id);
}

//=----------------- IR Interfaces ---------------=//
  void pcce_print_hello(short i) {
    printf("pcce hello %d!\n", i);
    fflush(stdout);
  }

void pcce_push_ctx(size_t site) {
  //printf("pcce push ctx\n");
  auto pa = make_pair(pcce_id, (short)site);
  _ctx_stack.push_back(pa);
  pcce_id = 0;
  _pushed++;
  
  // if (site == 2548) {
  //   pcce_print_hello();
  // }
}

void pcce_pop_ctx() {
  //printf("pcce pop ctx\n");

  assert(_ctx_stack.size() > 0);
  pcce_id = _ctx_stack[_ctx_stack.size() - 1].first;
  _ctx_stack.pop_back();
}

void pcce_pop_ctx1(size_t site) {
  //printf("pcce pop ctx\n");
  assert(_ctx_stack.size() > 0);
  pcce_id = _ctx_stack[_ctx_stack.size() - 1].first;

  assert(site == _ctx_stack[_ctx_stack.size() - 1].second);
  _ctx_stack.pop_back();
}

}
