//
// Created by tzhou on 3/9/18.
//

#include "Commons.hpp"

void bt_error_callback(void *data,
                       const char *msg,
                       int errnum) {
  fprintf(stderr, "BT Err: %s (%d)\n", msg, errnum);
}

std::string getBTCtxAsStr() {
  void *array[256];
  size_t size;
  char **strings;
  std::string ctx;
  size_t i;

  size = backtrace(array, 256);
  strings = backtrace_symbols(array, size);

  for (i = 0; i < size; i++) {
    //printf ("%s\n", strings[i]);
    ctx += std::string(strings[i]) + "\n";
  }

  free(strings);
  return ctx;
}