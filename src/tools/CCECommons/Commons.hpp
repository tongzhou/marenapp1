//
// Created by tzhou on 3/8/18.
//

#ifndef LIBMARENA_COMMONS_HPP
#define LIBMARENA_COMMONS_HPP

#include <cstdio>
#include <string>
#include <execinfo.h>

using namespace std;  // Use it anyway

void bt_error_callback(void *data,
                       const char *msg,
                       int errnum);

std::string getBTCtxAsStr();

#endif //LIBMARENA_COMMONS_HPP
