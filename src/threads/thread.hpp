//
// Created by tzhou on 11/5/17.
//

#ifndef MSI_THREAD_HPP
#define MSI_THREAD_HPP

#include <pthread.h>
#include <map>


#define PID_FORMAT "%p"

typedef pthread_t thread_id_t;


class OSThread;

class Thread {
public:
    enum ThreadType {
        listener_thread,
        sender_thread,
    };

    static const char * thread_type_str(ThreadType t) {
        switch(t){
            default:              return "UnknownThread";
        }
    }
protected:
    OSThread* _osthread;
    ThreadType _thread_type;
    bool _should_terminate;
public:
    // Constructor
    Thread(ThreadType);
    virtual ~Thread();

    void set_thread_type(ThreadType threadType)  { _thread_type = threadType; }
    void set_should_terminate(bool v=true)       { _should_terminate = v;}
    bool should_terminate();

    // initializtion
    void initialize_thread_local_storage();

    // thread entry point
    virtual void run();
    virtual void do_initialization();  // will block the parent thread until this is done
    void start();
    void join();

    // OSThread
    OSThread* osthread()                                    { return _osthread; }
    void set_osthread(OSThread* osThread)                   { _osthread = osThread; }
    bool set_as_starting_thread();
    thread_id_t pthread_id();

    // Testers

    virtual char* name() const { return (char*)"Unknown thread"; }

    static Thread* current();

};


class JavaThread;

class Threads {
    private:
    static std::map<pthread_t, Thread*> _threads_table; /* for current() */
public:
    static Thread* get_pthread_by_id(pthread_t id);
    static void register_thread(Thread*);

    static void start_thread(Thread* thread);
//private:
//    static JavaThread* _thread_list; /* the head of the list */
//    static int         _number_of_threads;
//    static int         _number_of_non_daemon_threads;
//    static int         _return_code;
//    static int         _thread_claim_parity;
//public:
//    // Thread management
//    // force_daemon is a concession to JNI, where we may need to add a
//    // thread to the thread list before allocating its thread object
//    static void add(JavaThread* p, bool force_daemon = false);
//    static void remove(JavaThread* p);
//    static bool includes(JavaThread* p);

};


#endif //MSI_THREAD_HPP
