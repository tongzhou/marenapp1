#include <interfaces.hpp>
#include "profileThread.hpp"
#include <marena.hpp>
#include <algorithm>
#include <cassert>

ProfileThread::ProfileThread(): Thread(Thread::listener_thread) {
    
}

ProfileThread::~ProfileThread() {

}


void ProfileThread::process(void*p, const char* comment) {
    //marena->_mem_access_count++;
    //return;

    
    auto chunk_addrs = marena->_chunk_addrs;
    auto low = std::lower_bound(chunk_addrs.begin(), chunk_addrs.end(), p);
    int pos = low - chunk_addrs.begin();
    if (p < chunk_addrs[pos]) {
        if (pos == 0) {
            return;
        }
        else {
            pos -= 1;
        }
    }
    else {
        /* p should = _chunk_addrs[pos] */
        assert(p == chunk_addrs[pos]);
    }
        
    ChunkInfo* ci = marena->_chunk_info_map[chunk_addrs[pos]];
    auto ai = ci->arena;
    if (p < (ci->start + ci->size)) {
        marena->_mem_access_count++;
        marena->_mem_access_count %= marena->_mem_sample_rate;
        if (marena->_mem_access_count != 0) {
            return;
        }

        marena->_alloc_log << comment << ' ' << ai->arena_id << " " << ci->start << " +" << (char*)p-(char*)ci->start << std::endl;
        return;
    }
    else {
        marena->_alloc_log << "I " << p << " " << chunk_addrs[0] << " " << marena->_chunk_upper_bound << std::endl;
    }
}

void ProfileThread::run() {
    zpl("Profile thread runs");
    auto& lock = marena->_profile_lock;
    auto& q = marena->_addr_queue;
    
    while (!_should_terminate) {
        lock->lock();
        while (q.empty()) {
            lock->wait();
        }
        void* item = q.front();
        q.pop();
        lock->unlock();

        process(item, "");
        if (marena->_done && q.empty()) {
            break;
        }
    }
    zpl("Profile thread exits");
}
