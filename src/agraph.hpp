
#ifndef MARENA_AGRAPH_H
#define MARENA_AGRAPH_H

#include <cstdlib>
#include <unordered_map>

struct ListNode {
    int id;
    size_t weight;
    ListNode(int i, size_t w): id(i), weight(w) {}
};

class AGraph {
    std::unordered_map<int, std::unordered_map<int, ListNode*> > _table; 
public:
    void create_vertex(int id);
    void inc_weight(int n1, int n2, int value);
    void print();
};


#endif
