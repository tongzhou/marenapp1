#include <cstdio>
#include <cstdlib>
#include <jemalloc/jemalloc.h>
#include <cstring>
#include <cassert>
#include <iomanip>
#include <sys/mman.h>  // for madvise
#include <algorithm>
#include <iostream>
#include "interfaces.hpp"
#include "marena.hpp"
#include "threads/profileThread.hpp"
#include "agraph.hpp"


//__thread size_t xps_ctx = 0;
//short xps_ctx[ctx_size];
//short xps_ctx1[ctx_size];

void *arena_extent_alloc(extent_hooks_t *extent_hooks,
                         void *new_addr, size_t size,
                         size_t alignment, bool *zero,
                         bool *commit, unsigned arena_ind);


Marena::Marena() {  
  _is_active = true;
  _cold_id = 0;
  _hot_id = 0;
  _arena_mode = -1; // only makes sense when 'x' is used
  _allocator = 'c'; // 'j': je_malloc, 'c': malloc, 'x': je_mallocx
  _record_ptrs = true;
  _record_arenas = false;
  _print_arena_id = false;
  _trace_alloc = false; // trace allocations/deallocations
  _trace_mem = false; // profile mode: trace memory accesses
  _debug_mem_trace = false;
  _guided = false;
  _shall_sample = false;
  _mem_sample_rate = 2000;
  _mem_access_count = 0;
  _chunk_upper_bound = NULL;
  _prev_ap = -1;
  //_done = false;

  _alloc_bytes = 0;

  if (char* env = getenv("XPS_DisableMarena")) { _is_active = false; }
  if (char* env = getenv("XPS_ArenaMode")) { _arena_mode = std::stoi(env); }
  if (char* env = getenv("XPS_Allocator")) { _allocator = env[0]; }
  if (char* env = getenv("XPS_RecordPtrs")) { _record_ptrs = true; }
  if (char* env = getenv("XPS_PrintArenaId")) { _print_arena_id = true; }


  if (_arena_mode == 1) {
    create_arena();  // only use one arena
  }

  if (_trace_mem) {
    _mem_log.open("mem.log");
    _graph = new AGraph();
  }

  if (_trace_alloc) {
    int width = 20;
    _alloc_log.open("alloc.log");
    // _alloc_log << std::left
    //            << std::setw(width) << "arena"
    //            << std::setw(width) << "apid"
    //            << std::setw(width) << "address"
    //            << std::setw(width) << "size"
    //            << std::setw(width) << "size(in hex)"
    //            << std::endl;

    // _profile_lock = new Monitor();
    // _profile_thread = new ProfileThread();
    // _profile_thread->start();
  }

  if (_allocator == 'x') {
    /* Attach the extent hooks for jemalloc */
    _mhook = new extent_hooks_t();
    memset(_mhook, 0, sizeof(extent_hooks_t));
    _mhook->alloc = arena_extent_alloc;
  }

  printf("marena active: %d\n"
             "arena mode: %d\n"
             "default allocator: %c\n"
             "trace_alloc: %d\n",
         _is_active, _arena_mode, _allocator, _trace_alloc);
  fflush(stdout);
}

Marena::~Marena() {
}

/**@brief The malloc family here allocates from a given arena and does some bookkeepping
 * toggoling set_is_active is unnecessary here because malloc is never overridden  
 *
 * @param size
 * @param arena_id
 * @return
 */
void* Marena::xps_malloc(ap_id_t ap_id, size_t size) {
  _alloc_bytes += size;
  if (size == 0) {
    printf("Size is 0!\n");
    return NULL;
  }

  void *ptr;

  switch (_allocator) {
  case 'c':
    ptr = malloc(size);
    break;
  case 'j':
    ptr = je_malloc(size);
    break;
  case 'x': {
    arena_id_t arena_id = 0;
    if (_arena_mode != 0) {
      arena_id = get_arena_id(ap_id);
    }
    if (_print_arena_id) {
      printf("arena id: %u\n", arena_id);
    }
    ptr = je_mallocx(size, MALLOCX_ARENA(arena_id) | MALLOCX_TCACHE_NONE); // have an issue with gcc
    break;
  }
  default:
    assert(0);
  }

  if (ptr && _record_ptrs) {
    add_ptr_record(ptr, size, ap_id);
  }

  return ptr;
}

void* Marena::xps_calloc(ap_id_t ap_id, size_t nmemb, size_t size) {
  _alloc_bytes += nmemb*size;
  void* ptr = xps_malloc(ap_id, nmemb*size);
  memset(ptr, 0, nmemb*size);
  return ptr;
}

void* Marena::xps_realloc(ap_id_t ap_id, void *old_ptr, size_t size) {
  _alloc_bytes += size;
  /* handle specical cases */
  if (!old_ptr) {
    return xps_malloc(ap_id, size);
  }

  if (size == 0) {
    xps_free(old_ptr);
    printf("Size is 0!\n");
    return NULL;
  }

  void* ptr;
  switch (_allocator) {
  case 'c':
    ptr = realloc(old_ptr, size);
    break;
  case 'j':
    ptr = je_realloc(old_ptr, size);
    break;
  case 'x': {
    arena_id_t arena_id = 0;
    if (_arena_mode != 0) {
      arena_id = get_arena_id(ap_id);
    }
    if (_print_arena_id) {
      printf("arena id: %u\n", arena_id);
    }
    ptr = je_rallocx(old_ptr, size, MALLOCX_ARENA(arena_id) | MALLOCX_TCACHE_NONE); // have an issue with gcc
    break;
  }
  default:
    assert(0);
  }

  if (_record_ptrs) {
    remove_ptr_record(old_ptr);
    if (ptr) {
      add_ptr_record(ptr, size, ap_id);
    }
  }

  return ptr;
}

void Marena::xps_free(void *ptr) {
  if (!ptr) {
    return;
  }

  if (_record_ptrs) {
    /* if the pointer is from library code,
     * they must be freed by free
     */
    if (!in_ptr_record(ptr)) {
      free(ptr);
      return;
    }
    else { /* if the pointer is managed by marena */
      remove_ptr_record(ptr);
      /* then do our deallocation */
    }
  }


  switch (_allocator) {
  case 'c':
    free(ptr);
    break;
  case 'j':
    je_free(ptr);
    break;
  case 'x':
    //je_dallocx(ptr, MALLOCX_ARENA(0) | MALLOCX_TCACHE_NONE); // have an issue with gcc
    je_dallocx(ptr, MALLOCX_TCACHE_NONE); // have an issue with gcc
    break;
  default:
    assert(0);
  }
}

arena_id_t Marena::get_arena_id(unsigned ap_id) {
  if (_arena_mode == 1) {
    //return _arenas.at(0);
    return 0;  // todo: not sure if this will work
  }
  else {

    // if (ap_id == 136 || ap_id == 140 || ap_id == 175) {
    //     ap_id == 119;
    // }

    if (_ap_map.find(ap_id) == _ap_map.end()) {
      auto ap = new APInfo(ap_id);
      ap->arena_id = create_arena();
      if (_trace_mem) {
        ArenaInfo* arena = get_arena_info(ap->arena_id);
        arena->ap_id = ap_id;
      }


      _ap_map[ap_id] = ap;
    }
    //printf("apid: %d, arena id: %d\n", ap_id, _ap_arena_map[ap_id]);

    auto info = _ap_map[ap_id];
    return info->arena_id;
  }
}

ArenaInfo* Marena::get_arena_info(arena_id_t id) {
  if (_arena_info_map.find(id) != _arena_info_map.end()) {
    return _arena_info_map[id];
  }
  else {
    std::cerr << "<error>: arena " + std::to_string(id) + " not found!";
    assert(0);
  }
}

arena_id_t Marena::create_arena() {
  arena_id_t id;
  size_t size = sizeof(arena_id_t);



  if (_trace_mem) {
    if (int err = je_mallctl("arenas.create", &id, &size, &_mhook, sizeof(_mhook))) {
      fprintf(stderr, "Making arena failed, error code: %d\n", err);
      return 0;
    }
  }
  else {
    if (int err = je_mallctl("arenas.create", &id, &size, NULL, 0)) {
      fprintf(stderr, "Making arena failed, error code: %d\n", err);
      return 0;
    }

  }


  return id;
}

void Marena::add_chunk_info(ChunkInfo *ci) {
  _chunk_info_map[ci->start] = ci;

  auto insert_pos = std::upper_bound(_chunk_addrs.begin(), _chunk_addrs.end(), ci->start);
  if (insert_pos == _chunk_addrs.end()) {
    _chunk_upper_bound = ci->start + ci->size;
    _chunk_addrs.push_back(ci->start);
  }
  else {
    _chunk_addrs.insert(insert_pos, ci->start);
  }

  if (_debug_mem_trace) {
    _mem_log << "C" << " " << ci->start << " " << ci->start+ci->size << std::endl;
  }
}

// AllocPoint* Marena::get_alloc_point_info(unsigned ap_id) {
//     AllocPoint* api = new AllocPoint();
//     api->id = ap_id;
//     return api;
// }

void Marena::log_alloc(PtrRecord* pr) {
  _alloc_log << "A " << pr->ap_id << " " << pr->key << " " << pr->size;
  if (_allocator == 'x') {
    _alloc_log << " " << get_arena_id(pr->ap_id);
  }
  _alloc_log << std::endl;
}

void Marena::log_free(PtrRecord* pr) {
  _alloc_log << "F " << pr->ap_id << " " << pr->key << " " << pr->size;
  if (_allocator == 'x') {
    _alloc_log << " " << get_arena_id(pr->ap_id);
  }
  _alloc_log << std::endl;
}

void Marena::add_ptr_record(void *ptr, size_t size, ap_id_t ap_id) {
  PtrRecord* pr = NULL;
  if (pr = _ptr_records[ptr]) {  // might happen with realloc
    pr->size = size;
    pr->ap_id = ap_id;
  }
  else {
    pr = new PtrRecord();
    pr->key = ptr;
    pr->size = size;
    pr->ap_id = ap_id;
    _ptr_records[ptr] = pr;
  }

  if (_trace_alloc) {
    log_alloc(pr);
    auto apinfo = _ap_map.at(ap_id);
    apinfo->size += size;
  }

  // if (_record_arenas) {
  //     auto pr = new PtrRecord();
  //     pr->key = ptr;
  //     pr->size = size;
  //     pr->ap_id = ap_id;
  //     printf("apid: %d\n", ap_id);
  //     auto arena_id = get_arena_id(ap_id);
  //     //_arena_records[arena_id].push_back(_ptr_records[ptr]);
  //     _arena_records[arena_id].push_back(pr);
  // }
}

void Marena::remove_ptr_record(void *ptr) {
  if (PtrRecord* pr = _ptr_records[ptr]) {
    if (_trace_alloc) {
      log_free(pr);
    }

    delete pr;
    // not iterate the map and erase to avoid some overhead
    // use "returning NULL" instead of "not exist" to check existance
    _ptr_records[ptr] = NULL;
  }
}

bool Marena::in_ptr_record(void *p) {
  return _ptr_records.find(p) != _ptr_records.end();
}


void Marena::record_mem_access(void *p, const char* comment) {
  // for (auto a: _chunk_addrs){
  //     std::cout << a << std::endl;
  // }
  // printf("\n");
  //exit(0);
  if (_trace_mem) {
    if (_chunk_addrs.empty()) {
      return;
    }

    if (p < _chunk_addrs[0]) {
      return;
    }

    if (p >= _chunk_upper_bound) {
      return;
    }

    //_mem_access_count++;
    //return;

    void* chunk;
    auto it = std::lower_bound(_chunk_addrs.begin(), _chunk_addrs.end(), p);
    if (it == _chunk_addrs.end()) {
      auto size = _chunk_addrs.size();
      chunk = _chunk_addrs[size-1];
    }
    else {
      int pos = it - _chunk_addrs.begin();
      void* low = _chunk_addrs.at(pos);
      if (p < low) {
        if (pos == 0) {
          return;
        }
        else {
          pos -= 1;
        }
      }
      else {
        /* p should = _chunk_addrs[pos] */
        assert(p == _chunk_addrs[pos]);
        // if (p != _chunk_addrs[pos]) {
        //     std::cout << "N " << p << " " << _chunk_addrs[pos] << std::endl;
        // }
      }
      chunk = _chunk_addrs[pos];
    }

    ChunkInfo* ci = _chunk_info_map[chunk];
    ArenaInfo* ai = ci->arena;
    if (p < (ci->start + ci->size)) {
      if (_shall_sample) {
        _mem_access_count++;
        _mem_access_count = _mem_access_count % _mem_sample_rate;
        if (_mem_access_count != 0) {
          return;
        }
      }

      int this_ap = ai->ap_id;
      if (_prev_ap > 0 && this_ap != _prev_ap) {
        _graph->inc_weight(_prev_ap, this_ap, 1);
        _graph->inc_weight(this_ap, _prev_ap, 1);
      }
      _prev_ap = this_ap;
      // _mem_log << comment << ' ' << ai->arena_id << " " << ci->start << " +" << (char*)p-(char*)ci->start << std::endl;

      return;
    }
    else {
      if (_debug_mem_trace) {
        //_mem_log << "I " << p << " " << ci->start << "~" << ci->start+ci->size << " " << ci->size << " " << _chunk_upper_bound << std::endl;
      }

    }

  }
}


//void* _Znam(size_t);

void* Marena::ctx_malloc(size_t size) {
  //pcce_check_ctx();
  //return malloc(size);
  return malloc(size);
}

void* Marena::ctx_calloc(size_t nmemb, size_t size) {
  //check_ctx(7);
  //pcce_check_ctx();
  return calloc(nmemb, size);
}

void* Marena::ctx_realloc(void *ptr, size_t size) {
  //pcce_check_ctx();
  return realloc(ptr, size);
}

void Marena::ctx_free(void *ptr) {
  //hcce_check_ctx(13);
  free(ptr);
}

void Marena::record_load(void *p) {
  record_mem_access(p, "L");
}

void Marena::record_store(void *p) {
  record_mem_access(p, "S");
}

void Marena::print_stats() {
  print_proc_stats();
  if (_trace_mem) {
    print_arena_aps();
    _graph->print();
  }
}

void Marena::print_arena_aps() {
  printf("in arena_aps\n");
  std::ofstream ofs("arenas.out");
  for (auto it: _ap_map) {
    APInfo* info = it.second;
    ofs << it.first << " " << info->arena_id << " " << info->size << std::endl;
  }
  ofs.close();
}

void Marena::print_proc_stats() {
  // marena stats
  FILE *fptr1, *fptr2;
  char filename[100], c;

  // Open one file for reading
  fptr1 = fopen("/proc/self/status", "r");
  if (fptr1 == NULL)
  {
    printf("Cannot open file %s \n", filename);
    exit(0);
  }

  // Open another file for writing
  fptr2 = fopen("proc_stats.out", "w");
  if (fptr2 == NULL)
  {
    printf("Cannot open file %s \n", filename);
    exit(0);
  }

  // Read contents from file
  c = fgetc(fptr1);
  while (c != EOF)
  {
    fputc(c, fptr2);
    c = fgetc(fptr1);
  }

  fclose(fptr1);
  fclose(fptr2);
}

// void Marena::print_ptrs_by_arena() {
//     printf("in print_ptrs_by_arena\n");
//     const char* filename = "arena_stats.out";
//     std::ofstream ofs(filename);
//     if (!ofs.good()) {
//         fprintf(stderr, "open file %s failed\n", filename);
//     }

//     int width = 20;
//     ofs << std::left << std::setw(width) << "arena"
//         << std::setw(width) << "apid"
//         << std::setw(width) << "address"
//         << std::setw(width) << "size"
//         << std::setw(width) << "size(in hex)"
//         << std::endl;
// //    for (auto it: _arena_records) {
// //        //fprintf(fp, "arena %d\n", it.first);
// //        auto prs = it.second;
// //        for (auto pr: prs) {
// //            ofs << std::left << std::setw(width) << it.first
// //                << std::setw(width) << pr->ap_id
// //                << std::setw(width) << pr->key
// //                << std::setw(width) << pr->size
// //                << std::hex << pr->size << std::dec
// //                << std::endl;
// //
// //            //fprintf(fp, "arena %p, %d, %d\n", pr->key, pr->size, pr->ap_id);
// //        }
// //        ofs << "\n";
// //    }

// }





void *arena_extent_alloc(extent_hooks_t *extent_hooks,
                         void *new_addr, size_t size,
                         size_t alignment, bool *zero,
                         bool *commit, unsigned arena_ind) {
  int err;
  void *addr = NULL;
  ChunkInfo* chunk = new ChunkInfo();


  //printf("extend_alloc called: size: %zu arena: %d\n", size, arena_ind);

  auto& ai_map = marena->arena_info_map();

  if (ai_map.find(arena_ind) == ai_map.end()) {
    auto ai = new ArenaInfo(arena_ind);

    printf("added arena %d\n", arena_ind);
    ai_map[arena_ind] = ai;
  }

  addr = mmap(new_addr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (addr == MAP_FAILED) {
    return NULL;
  }

  chunk->arena = marena->get_arena_info(arena_ind);
  chunk->start = addr;
  chunk->size = size;
  chunk->arena->size += size;
  chunk->arena->nchunk++;

  marena->add_chunk_info(chunk);


  *zero = 0;
  *commit = 1;

  return addr;
}

//bool arena_extent_purge(extent_hooks_t *extent_hooks,
//                        void *addr, size_t size, size_t offset,
//                        size_t length, unsigned arena_ind) {
//    int err;
//    err = madvise(((char *)addr) + offset, length, MADV_DONTNEED);
//    return (err != 0);
//}

