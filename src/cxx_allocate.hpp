//
// Created by tzhou on 3/14/18.
//

#ifndef LIBMARENA_CXX_ALLOCATE_HPP
#define LIBMARENA_CXX_ALLOCATE_HPP

extern "C" {
void* _Znam(size_t);
void* _Znwm(size_t);
void* _Znaj(size_t);
void* _Znwj(size_t);
void* _ZdaPv(size_t);
void* _ZdlPv(size_t);
};

#endif //LIBMARENA_CXX_ALLOCATE_HPP
