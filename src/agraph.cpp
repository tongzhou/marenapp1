
#include <fstream>
#include "agraph.hpp"

void AGraph::create_vertex(int id) {
    
}

void AGraph::inc_weight(int n1, int n2, int value) {
    if (_table.find(n1) == _table.end()) {
        _table[n1] = std::unordered_map<int, ListNode*>();
        //printf("create empty list for %d\n", n1);
    }

    auto& neighbors = _table.at(n1);
    if (neighbors.find(n2) == neighbors.end()) {
        neighbors[n2] = new ListNode(n2, 0);
        //printf("create list node %d for %d\n", n2, n1);
    }

    auto node = neighbors.at(n2);
    node->weight += value;
}

void AGraph::print() {
    std::ofstream ofs("graph.out");
    for (auto v: _table) {
        for (auto it: v.second) {
            ListNode* node = it.second;
            ofs << v.first << " " << node->id << " "
                << node->weight << std::endl;
        }
    }
    ofs.close();
}
