//
// Created by tzhou on 9/28/17.
//

#ifndef LIBMARENA_INTERFACES_HPP_HPP
#define LIBMARENA_INTERFACES_HPP_HPP


#include <cstdlib>

class Marena;

extern Marena* marena;

extern "C" void *__libc_malloc(size_t size);
extern "C" void *__libc_calloc(size_t nmemb, size_t size);
extern "C" void *__libc_realloc(void *ptr, size_t size);
extern "C" void *__libc_free(void *ptr);
extern "C" void printBacktrace(int magic);

#endif //LIBMARENA_INTERFACES_HPP_HPP
